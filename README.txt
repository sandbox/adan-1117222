$Id$

Description
-----------
This module link a node with a webform submission, as a way of extend the
information of a node, and providing a method for dynamic content subtypes.

It is useful for scenarios like websites where users may submit request,
with an additional (optional) datasheet, with different fields based on
request parameters.

It also provides website users the ability of manage content types without
the complexity of CCK, thank's to the webform (3.x) UI.

Requirements
------------
Drupal 6.x
Webform module, version 6.x-3.x

Installation and use
--------------------
1. Install and enable the module as described at
   http://drupal.org/getting-started/install-contrib/modules

2. Create at least one webform-enabled node (at /node/add/webform) with
   some fields

3. Edit settings of content type you want to enable the webform
   submission attachment feature, eg., for page type, go to
   /admin/content/node-type/page. Enable the "Webform submission attachments"
   at "Workflow settings"

4. Create your first "wfsa" page at /node/add/page/#nid_of_your_webform.
   Eg., if your webform has nid 1, try /node/add/page/1

Notes
-----
* removing nodes with attached submission will remove the submission as well

* removing the attached submission of a node will not remove the node
  itself, but will de-attach the submission from the node

* attached submission can be edited when editing the node (to users with
  "edit all/own webform attachments" permission)

* webform pagination is not taken into account

* there are permissions to create webform attachments, edit own/all
  webform attachments, and view own/all webform attachments. Existent
  webform permission policy is not taken into account, except "view"
  node webform, needed when creating new attachments.

Support
-------
Please use the issue queue for filing bugs with this module at
http://drupal.org/project/issues/wfsa
