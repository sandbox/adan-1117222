<?php
// $Id$

/**
 * @file
 * Include file for node/add/#type and /node/#nid/edit menu hooks.
 */

// load both node & webform required includes for node edition/creation form
module_load_include('inc', 'node', 'node.pages');
module_load_include('inc', 'webform', 'includes/webform.components');

// some components have required includes before form construction
webform_component_include('date');
